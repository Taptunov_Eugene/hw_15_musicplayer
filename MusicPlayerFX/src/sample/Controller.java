package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

public class Controller {

    private final ObservableList<String> genres = FXCollections.observableArrayList("Ambient",
            "Doom metal", "Black metal", "Death Metal", "Folk Metal", "Grunge", "Punk Rock",
            "Nu-Metal", "Gothic Metal", "Dark Electro");

    private final ObservableList<String> ambient = FXCollections.observableArrayList("Black Autumn",
            "Poenarian Mist", "Burzum");
    private final ObservableList<String> doomMetal = FXCollections.observableArrayList("Saint Vitus",
            "Witchcraft", " Orodruin");
    private final ObservableList<String> blackMetal = FXCollections.observableArrayList("Malsain",
            "Mordichrist", "Dimmu Borgir");
    private final ObservableList<String> deathMetal = FXCollections.observableArrayList("Arch Enemy",
            "Behemoth", "Devil Driver");
    private final ObservableList<String> folkMetal = FXCollections.observableArrayList("Finntroll",
            "In Extremo", "Schelmish");
    private final ObservableList<String> grunge = FXCollections.observableArrayList("Creed",
            "Nirvana", "Seether");
    private final ObservableList<String> punkRock = FXCollections.observableArrayList("Blink-182",
            "The Offspring", "Iggy Pop");
    private final ObservableList<String> nuMetal = FXCollections.observableArrayList("Limp Bizkit",
            "Korn", "Linkin Park");
    private final ObservableList<String> gothicMetal = FXCollections.observableArrayList("Nightwish",
            "Therion", "Nightwish");
    private final ObservableList<String> darkElectro = FXCollections.observableArrayList("Alien Vampires",
            "Eden Synthetic Corps", "Hocico");


    @FXML
    private ComboBox<String> musicianChooser;

    @FXML
    private TableColumn<?, ?> showGenre;

    @FXML
    private ComboBox<String> genreChooser;

    @FXML
    private TableColumn<?, ?> showMusician;

    @FXML
    private Button playMusic;

    @FXML
    private TableView<String> tracksTable;

    @FXML
    private TableColumn<?, ?> showTracks;

    @FXML
    private Button tracksShowButton;

    @FXML
    private Button stopMusic;

    @FXML
    void initialize() {
        //showGenre.setCellValueFactory(new PropertyValueFactory<?, ?>("genre"));
        genreChooser.setItems(genres);
        genreChooser.setOnAction(actionEvent -> {
            switch (genreChooser.getValue()) {
                case "Ambient":
                    musicianChooser.setItems(ambient);
                case "Doom metal":
                    musicianChooser.setItems(doomMetal);
                case "Black metal":
                    musicianChooser.setItems(blackMetal);
                case "Death Metal":
                    musicianChooser.setItems(deathMetal);
                case "Folk Metal":
                    musicianChooser.setItems(folkMetal);
                case "Grunge":
                    musicianChooser.setItems(grunge);
                case "Punk Rock":
                    musicianChooser.setItems(punkRock);
                case "Nu-Metal":
                    musicianChooser.setItems(nuMetal);
                case "Gothic Metal":
                    musicianChooser.setItems(gothicMetal);
                case "Dark Electro":
                    musicianChooser.setItems(darkElectro);
            }
        });
    }

}
